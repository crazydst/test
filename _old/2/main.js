var resourceList = [[0], [0, 10, 1.2], [0, 10, 1.2], [0, 10, 1.2]];

function setAlignCell(_cell)
{
	_cell.style.textAlign = "center";
	_cell.style.verticalAlign = "center";	
}

function nFormatter(num, digits) {
  var si = ["", "k", "M", "G", "T", "P", "E"];
  if (num == 0)
	  return 0;
  var expID;
  if(num < 10)
	  expID = 0;
  else
	expID = Math.floor(Math.floor(Math.log(num)/Math.log(10))/3);
  return (num / Math.pow(10, expID*3)).toFixed(digits) + si[expID];
}

function tableClick(_td, column)
{
	var _row = _td.rows[0];
	if(column == 0)
	{
		resourceList[0][0]++;
		_row.cells[0].innerHTML = nFormatter(resourceList[0][0], 3).toString();
	}
	else
	{
		if(resourceList[column-1][0]>=resourceList[column][1])
		{
			resourceList[column-1][0]-=resourceList[column][1];
			resourceList[column][0]++;
			resourceList[column][1]*=resourceList[column][2];
			resourceList[column][1]=Math.round(resourceList[column][1]);
			
			if(column == 1)
				_row.cells[column-1].innerHTML = nFormatter(resourceList[column-1][0], 3).toString();
			else
			{
				_row.cells[column-1].children[0].rows[0].cells[1].innerHTML = (resourceList[column-1][1]).toString();
				_row.cells[column-1].children[0].rows[1].cells[1].innerHTML = nFormatter(resourceList[column-1][0], 3).toString();
			}
			_row.cells[column].children[0].rows[0].cells[1].innerHTML = (resourceList[column][1]).toString();
			_row.cells[column].children[0].rows[1].cells[1].innerHTML = nFormatter(resourceList[column][0], 3).toString();			
		}
	}
	
}

function updateTable(curTable, tick)
{
	var _row = curTable.rows[0];	
	for(var i=3; i>1 ; i--)
	{
		resourceList[i-1][0] += 1.5*resourceList[i][0]/tick;
		_row.cells[i-1].children[0].rows[1].cells[1].innerHTML = nFormatter(resourceList[i-1][0], 3).toString();
	}
	
	resourceList[0][0] += 1.5*resourceList[1][0]/tick;
	_row.cells[0].innerHTML = nFormatter(resourceList[0][0], 3).toString();	
}

function SetTableStyleContent(curTable)
{
	var i=0;
	var cTable;
	var cRow;
	setAlignCell(curTable.rows[0].cells[0]);
	for(i = 1; i < curTable.rows[0].cells.length; i++)
	{
		var cTable = document.createElement('table');
		cRow = cTable.appendChild(document.createElement('tr'));
		cRow.appendChild(document.createElement('td'));
		cRow.appendChild(document.createElement('td'));
		cRow = cTable.appendChild(document.createElement('tr'));
		cRow.appendChild(document.createElement('td'));
		cRow.appendChild(document.createElement('td'));		
		curTable.rows[0].cells[i].appendChild(cTable);

		setAlignCell(curTable.rows[0].cells[i].children[0].rows[0]);		
		setAlignCell(curTable.rows[0].cells[i].children[0].rows[1]);
		
		curTable.rows[0].cells[i].children[0].rows[0].cells[0].innerHTML = "up cost:";
		curTable.rows[0].cells[i].children[0].rows[1].cells[0].innerHTML = "prod/s:";		
		curTable.rows[0].cells[i].children[0].rows[1].cells[1].innerHTML = resourceList[i][0];
		curTable.rows[0].cells[i].children[0].rows[0].cells[1].innerHTML = resourceList[i][1];
		
	}	
	
	
	
	curTable.rows[0].cells[0].onclick = function()	{
														var _t = curTable;
														return tableClick(_t, 0);
													}
	curTable.rows[0].cells[1].onclick = function()	{
														var _t = curTable;
														return tableClick(_t, 1);
													}
	curTable.rows[0].cells[2].onclick = function()	{
														var _t = curTable;
														return tableClick(_t, 2);
													}
	curTable.rows[0].cells[3].onclick = function()	{
														var _t = curTable;
														return tableClick(_t, 3);
													}													
}

function cMainTable()
{
	var rows = 2;
	var columns = 2;
	var tableDim = [[[80, 80], [20, 80]], [[80, 20], [20, 20]]];
	
	var x = document.createElement('table');
	document.body.appendChild(x);	
	x.style.borderCollapse = "collapse";
//	x.style.width = "100%";
//	x.style.height = "100%";
	
	for (var i=0; i< rows; i++)
	{
		var _tr = x.appendChild(document.createElement('tr'));
		for(var j=0; j<columns; j++)
		{
				var _td = _tr.appendChild(document.createElement('td'));
//				_td.style.width = tableDim[i][j][0].toString() + "%";
//				_td.style.height= tableDim[i][j][1].toString() + "%";
//				_td.style.border = "1px solid #000000";
		}
	}

//	SetTableStyleContent(x);	
	
	return x;	
}

function cMainTableResize(curTable)
{
	var rows = 2;
	var columns = 2;
	var tableDim = [[[80, 80], [20, 80]], [[80, 20], [20, 20]]];
	
//	curTable.style.width = "100%";
//	curTable.style.height = "100%";
	var parent = document.body;
	parent.style.width = window.innerWidth.toString() + "px";
	parent.style.height = window.innerHeight.toString() + "px";
	
	var _tr, _td;
	for (var i=0; i< rows; i++)
	{
		_tr = curTable.rows[i];
		for(var j=0; j<columns; j++)
		{
				_td = _tr.cells[j];
				_td.style.width = (Math.floor(tableDim[i][j][0]/100*parseInt(parent.style.width, 10))).toString() + "px";
				_td.style.height= (Math.floor(tableDim[i][j][1]/100*parseInt(parent.style.height, 10))).toString() + "px";
		}
	}

}

function addImageToTable(_td, imgPath)
{
	var img = document.createElement("img");
	img.src = imgPath;
	img.style.width = "100%";
	img.style.height = "100%";
	_td.appendChild(img);	
}

function cTable(parent, columns = 3, rows = 3, _style = "1px solid #000000", labirynthArr = undefined)
{
	
	var x = document.createElement('table');
	parent.appendChild(x);
	
	x.style.borderCollapse = "collapse";
	x.parent = parent;

	if(labirynthArr)
		rows = labirynthArr.length;
	
	for (var i=0; i< rows; i++)
	{
		var _tr = x.appendChild(document.createElement('tr'));
		if(labirynthArr)
			columns = labirynthArr[i].length;
		for(var j=0; j<columns; j++)
		{
				var _td = _tr.appendChild(document.createElement('td'));
				_td.style.border = _style;
				if(labirynthArr)
				{
					if(labirynthArr[i][j] == 1)
					{
						_td.style.padding = "0";								
						addImageToTable(_td, "./img/fill.png");
					}
				}
		}
	}

//	SetTableStyleContent(x);	
	return x;
	
}

function tableResize(curTable)
{
	var square = Math.floor(parseInt(curTable.parent.style.height, 10)/curTable.rows.length);
	if(parseInt(curTable.parent.style.width, 10)/curTable.rows[0].cells.length < square)
		square = Math.floor(parseInt(curTable.parent.style.width, 10)/curTable.rows[0].cells.length);
		
	for (var i=0; i< curTable.rows.length; i++)
	{
		for(var j=0; j<curTable.rows[0].cells.length; j++)
		{
			curTable.rows[i].cells[j].style.width = square.toString() + "px";
			curTable.rows[i].cells[j].style.height = square.toString() + "px";
		}
	}		
}

function getEmptyTd(table)
{
	for(var i = 0; i < table.rows.length; i++)
	{
		for(var j=0; j < table.rows[i].cells.length; j++)
		{
			if (table.rows[i].cells[j].children.length == 0)
				return table.rows[i].cells[j];
		}
	}
}

class actionButton
{
		constructor(availTable, actionTable, actionName)
		{
			this.curActionID = allActions[actionName][1];
			this.imgSrc = allActions[actionName][0];
			this.parentTd = getEmptyTd(availTable);
			this.actionTable = actionTable;
			
			addImageToTable(this.parentTd, this.imgSrc);
			
			this.parentTd.addEventListener('click', this.addAction.bind(this));
		}

		addAction()
		{
			addImageToTable(getEmptyTd(this.actionTable), this.imgSrc);
		}
		
		addEvent()
		{
//			this.parentTd.onclick = function() { return this.addAction(); }
			this.parentTd.addEventListener("click", this.addAction);
		}
}


var h = document.createElement('head');
h.innerHTML = "test";
document.documentElement.appendChild(h);
var b = document.createElement('body');
document.documentElement.appendChild(b);
b.style.overflow = "hidden";

var mainTable = cMainTable();

var level = 0;

var cPlayT = cTable(mainTable.rows[0].cells[0], 20, 20, "0px dashed DarkGrey", levelConfig[level]["design"]);
var cProgT = cTable(mainTable.rows[0].cells[1], 2, 5, "1px dashed DarkGrey");
var cChooseT = cTable(mainTable.rows[1].cells[0], 5, 2, "1px dashed DarkGrey");

window.onresize = function()
{
	cMainTableResize(mainTable);
	tableResize(cPlayT);
	tableResize(cProgT);
	tableResize(cChooseT);
}

window.onresize();
var buttons = [];
for(var i=0; i < levelConfig[level]["actions"].length; i++)
{
	buttons[i] = new actionButton(cProgT, cChooseT, levelConfig[level]["actions"][i]);
}

//var tick = 5;

//var timer = setInterval(function(){updateTable(cT, tick);}, 1000/tick);
