var levelConfig = [];

var allActions ={	"leftArrow": ["./img/leftArrow.png", 0], 	// 0
					"rightArrow": ["./img/rightArrow.png", 1], 	// 1
					"upArrow": ["./img/upArrow.png", 2],		// 2
					"downArrow": ["./img/downArrow.png", 3],	// 3
				}

///////////////////////////////////////////////////////////////////////////
levelConfig[0] = {};
levelConfig[0]["design"] = 	[ 	[1, 1, 1, 1, 1, 1, 1],
								[1, 0, 1, 0, 0, 0, 1],
								[1, 0, 1, 0, 1, 0, 1],
								[1, 0, 0, 0, 1, 0, 1],
								[1, 1, 1, 1, 1, 1, 1],
							];
levelConfig[0]["actions"] = ["leftArrow", "rightArrow", "upArrow", "downArrow"];
							
levelConfig[1] = {};
levelConfig[1]["design"] = 	[ 	[1, 1, 1, 1, 1],
								[1, 0, 1, 0, 1],
								[1, 0, 0, 0, 1],
								[1, 1, 1, 1, 1],
							];
levelConfig[1]["actions"] = ["leftArrow", "rightArrow", "upArrow", "downArrow"];	