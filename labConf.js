var levelConfig = [];

var allActions ={	"leftArrow": ["./img/leftArrow.png", 0], 					// 0
					"rightArrow": ["./img/rightArrow.png", 1], 					// 1
					"upArrow": ["./img/upArrow.png", 2],						// 2
					"downArrow": ["./img/downArrow.png", 3],					// 3
					"leftArrow_acc": ["./img/leftArrow_accelerator.png", 4],	// 4
					"rightArrow_acc": ["./img/rightArrow_accelerator.png", 5],	// 5
					"upArrow_acc": ["./img/upArrow_accelerator.png", 6],		// 6
					"downArrow_acc": ["./img/downArrow_accelerator.png", 7]		// 8
				}

var moveObjPath = {"circle": "./img/active_obj.png"};
///////////////////////////////////////////////////////////////////////////
levelConfig[0] = {};
levelConfig[0]["design"] = 	[ 	[1, 1, 1, 1, 1, 1, 1],
								[1, 0, 1, 0, 0, 0, 1],
								[1, 0, 1, 0, 1, 0, 1],
								[1, 0, 0, 0, 1, 0, 1],
								[1, 1, 1, 1, 1, 1, 1],
							];
levelConfig[0]["start"] = [1, 1];
levelConfig[0]["obj"] = ["circle"];
levelConfig[0]["actions"] = [	"leftArrow", "leftArrow_acc", 
								"rightArrow", "rightArrow_acc",
								"upArrow", "upArrow_acc", 
								"downArrow", "downArrow_acc" ];
							
levelConfig[1] = {};
levelConfig[1]["design"] = 	[ 	[1, 1, 1, 1, 1],
								[1, 0, 1, 0, 1],
								[1, 0, 0, 0, 1],
								[1, 1, 1, 1, 1],
							];
levelConfig[1]["actions"] = ["leftArrow", "rightArrow", "upArrow", "downArrow"];
levelConfig[1]["start"] = [1, 1];
levelConfig[1]["obj"] = ["circle"];

levelConfig[2] = {};
levelConfig[2]["design"] = 	[ 	[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
								[1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1],
								[1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
								[1, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1],
								[1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1],
								[1, 1, 1, 0, 1, 1, 1, 0, 0, 0, 1],
								[1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1],
								[1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1],
								[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
							];
levelConfig[2]["start"] = [5, 6];
levelConfig[2]["obj"] = ["circle"];
levelConfig[2]["actions"] = [	"leftArrow", "leftArrow_acc", 
								"rightArrow", "rightArrow_acc",
								"upArrow", "upArrow_acc", 
								"downArrow", "downArrow_acc" ];
