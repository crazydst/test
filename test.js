class pObj
{
	constructor(elemType, _XobjID, _maxElements = undefined, hasContent = true) // 
	{
		this.pXobjID = true; // identificator parent as p<X> class
		
		this.XobjID = _XobjID;
		if(hasContent)
		{
			this.objLeftPos = 5;
			this.objRightPos = 5;
			this.objTopPos = 5;
			this.objBottomPos = 5;
			this.objSpace = 5;
		}
		else
		{
			this.objLeftPos = 0;
			this.objRightPos = 0;
			this.objTopPos = 0;
			this.objBottomPos = 0;
			this.objSpace = 0;
		}

		this.pXobjTree = [];		
		
		this.defaultWidth = 100;
		this.defaultHeight = 100;
		
		this.setDefaultVisibleSize();
		this.setRowColumn(); // delafult row = 1, column = 1
		this.maxEelements = _maxElements; // max elements in tree may be unlimited if undefined
		
		this.Xobj = document.createElement(elemType);
		this.Xobj.id = this.XobjID;
		this.Xobj.style.position = "absolute";
		this.Xobj.style.padding = "0";
//		this.Xobj.style.border = "0px solid white";				
	}
	
	setPosition(_top, _left, _right, _bottom)
	{
		this.Xobj.style.top = _top;
		this.Xobj.style.left = _left;
		this.Xobj.style.right = _right;
		this.Xobj.style.bottom = _bottom;
	}
	setBorderStyle(_border)
	{
		if(this.Xobj)
			this.Xobj.style.border = _border;		
	}
	
	setRowColumn(_rows = 1, _columns = 1)
	{
		this.rows = _rows; 	// may be undefined		
		this.columns = _columns; // may be undefined

		this.rowPos = 0; // currnt position in parent array
		this.columnPos = 0; // current position in parent array
		
		this.resizeMinVisibleElementSize();

		if(this.parent.pXobjID != undefined) // this.parent is pXobj class
		{
			this.parent.rearrangePositions();
		}		
	}
	
	resizeMinVisibleElementSize()
	{
		var objSquareX = 0, objSquareY = 0;

		if(this.columns != undefined)
			objSquareX = Math.floor((this.width - (this.objLeftPos + this.objRightPos) - this.objSpace*(this.columns - 1)) / this.columns);
		else
			objSquareX = Math.floor(this.width - (this.objLeftPos + this.objRightPos));
		if(this.rows != undefined)
			objSquareY = Math.floor((this.height - (this.objTopPos + this.objBottomPos) - this.objSpace*(this.rows - 1)) / this.rows);
		else
			objSquareY = Math.floor(this.height - (this.objTopPos + this.objBottomPos));		
		
		if(objSquareX > objSquareY)
			this.minObjSquare = objSquareY;
		else		
			this.minObjSquare = objSquareX;			
	}
	
	setVisibleSize(_width, _height)
	{
		this.width = _width;
		this.height = _height;
		
		this.resizeMinVisibleElementSize();
		this.resize();

	}
	
	setDefaultVisibleSize()
	{
		this.width = this.defaultWidth;
		this.height = this.defaultHeight;
		
		this.resizeMinVisibleElementSize();
		this.resize();		
	}	
	
	add(_parent, _insertInd = -1) 
	{
		this.parent = _parent;
		
		if(this.parent.pXobjID == undefined) // this.parent is not pXobj class
		{
			this.parent.appendChild(this.Xobj);
			this.mainXobj = this;
		}
		else // this.parent is pXobj class
		{
			if(this.parent.maxEelements != undefined)
			{
				if(this.parent.pXobjTree.length + 1 >= this.parent.maxEelements)
					// exceed elements;
					return 0;
			}

			this.mainXobj = this.parent.mainXobj;
			this.parent.Xobj.appendChild(this.Xobj);
				
			if(_insertInd != -1) 
				this.parent.pXobjTree.splice(_insertInd, 0, this);
			else
				this.parent.pXobjTree.push(this);
			
			this.parent.rearrangePositions();
		}	
		
		return 1;
	}
	
	rearrangePositions()
	{

		var curRow = 0, curColumn = 0;
		var curObj;
		for(var i = 0; i < this.pXobjTree.length; i++)
		{
			curObj = this.pXobjTree[i];
			
			if(curObj.rows == 1 && curObj.columns == 1) // button obj
			{
				curObj.rowPos = curRow;
				curObj.columnPos = curColumn;				
				curColumn++;
				if(this.columns != undefined)
				{
					if(curColumn > this.columns)
					{
						curRow++;
						curColumn = 0;
					}
				}
			}
			else // multiple obj starts with new row
			{
				if(curColumn > 0)
				{
					curRow++;
					curColumn = 0;
				}
				
				curObj.rowPos = curRow;
				curObj.columnPos = curColumn;				
				
				curRow++;
				curColumn = 0;
			}
		}
	}
	
	resize()
	{
		var i=0;
		var curRow = 0, curColumn = 0, maxColumn = 0;
		
		var childObjSize = [];
		var curXobj, prevXobj;
		var curWidth = 0, curHeight = 0;
		
		this.width = 0;
		this.height = 0;
		
		if(this.pXobjTree.length > 0)
		{
			for(i = 0; i < this.pXobjTree.length; i++)
			{
				curXobj = this.pXobjTree[i];
				if(i > 0)
				{	
					prevXobj = this.pXobjTree[i - 1];
					if(curXobj.rowPos == prevXobj.rowPos)
					{
						curXobj.Xobj.style.top = prevXobj.Xobj.style.top;
						curXobj.Xobj.style.left = (parseInt(prevXobj.Xobj.style.left, 10) + parseInt(prevXobj.Xobj.style.width, 10) + this.objSpace).toString() + "px";
					}
					else
					{
						curXobj.Xobj.style.left = this.objLeftPos.toString() + "px";
						curXobj.Xobj.style.top = (parseInt(prevXobj.Xobj.style.top, 10) + parseInt(prevXobj.Xobj.style.height, 10) + this.objSpace).toString() + "px";
					}
				}
				else
				{
					curXobj.Xobj.style.left = this.objLeftPos.toString() + "px";
					curXobj.Xobj.style.top = this.objTopPos.toString() + "px";
				}
				
				curXobj.minObjSquare = this.minObjSquare;				
				
				curXobj.resize();
				
				curWidth = parseInt(cuXobj.Xobj.style.left, 10) + parseInt(cuXobj.Xobj.style.width, 10);
				curHeight = parseInt(cuXobj.Xobj.style.top, 10) + parseInt(cuXobj.Xobj.style.height, 10);
				if(curWidth > maxWidth)
					maxWidth = curWidth;
				if(curHeight > maxHeight)
					maxHeight = curHeight;
			}
		
		}
		else
		{
			maxWidth = this.minObjSquare;
			maxHeight = this.minObjSquare;
		}

		if(this.minObjSquare == undefined)
			setDefaultVisibleSize();
		else
		{
			this.width = maxWidth;
			this.height = maxHeight;	
		}
		
		this.Xobj.style.height = this.height.toString() + "px";
		this.Xobj.style.width = this.width.toString() + "px";		
	}
	
}

class Btn extends pObj
{
	constructor(_XobjID)
	{
		super("button", _XobjID, 1, false);
	}
	
}

class Div extends pObj
{
	constructor(_XobjID, _row = undefined, _column = undefined)
	{
		super("div", _XobjID, 10);
		this.setRowColumn(_row, _column);
		this.Xobj.style.width = "100px";
		this.Xobj.style.height = "200px";
		this.setBorderStyle("2px dashed blue");	
	}
}

class DivInc extends pObj
{
	constructor(_XobjID, _row = undefined, _column = undefined)
	{
		super("div", _XobjID, 10);
		this.setRowColumn(_row, _column);
		this.setBorderStyle("1px dashed green");	
	}
}

var b = document.createElement('body');

document.documentElement.appendChild(b);

var d1 = new Div("d1", 5, 2);
d1.setPosition(10, 10, undefined, undefined);
d1.add(b);

var b1 = new Btn("b1");
b1.add(d1);

var b2 = new Btn("b2");
b2.add(d1);

var b3 = new Btn("b3");
b3.add(d1);


var d2 = new Div("d2", undefined, 4);
d2.setPosition(10, 300, undefined, undefined);
d2.add(b);

var b21 = new Btn("b21");
b21.add(d2);

var b22 = new Btn("b22");
b22.add(d2);

var d3 = new DivInc("d3", 4, 4);
d3.add(d2);
var b31 = new Btn("b31");
b31.add(d3);

var b32 = new Btn("b32");
b32.add(d3);

var b33 = new Btn("b33");
b33.add(d3);

var b23 = new Btn("b23");
b23.add(d2);

d1.resize();
d2.resize();