var fps = 25;
var percentSpeed = 200; //percent per sec

class ActionAccLeft
{
	constructor(_buttonClass)
	{
		this.buttonClass = _buttonClass;
		this.imgPath = allActions["leftArrow_acc"][0];

		this.playClass = this.buttonClass.playClass;
		this.moveObj = this.playClass.moveObj;
		this.objTr = this.playClass.table.rows[ this.playClass.moveObjCoord[1] ];
		this.objTd = this.objTr.cells[ this.playClass.moveObjCoord[0] ];
		this.finishID = undefined;
		this.finishCoord = undefiend;
		this.stepPerFps.undefined;
		
		this.percentShiftTop = this.playClass.moveObjImgLeftTopPercent[1];
		this.percentShiftLeft = this.playClass.moveObjImgLeftTopPercent[0];		
	}
	
	getImgPath()
	{
		return this.imgPath;
	}
	
	exec()
	{
		var moveObjCoord = this.playClass.moveObjCoord;
		if(moveObjCoord == undefined)
			return;		
		
		var borderInd = 1;		
		var borderTd = this.objTr.cells[ moveObjCoord[0]+borderInd ];
		
		while(borderTd.children.length == 0 && moveObjCoord[0]+borderInd < _tr.cells.length )
		{
			borderInd++;
			borderTd = this.objTr.cells[ moveObjCoord[0]+borderInd ];
		}
		
		this.finishID = borderInd-1;
		if(this.finishID > 0)
		{		
			this.finishTd = this.playClass.table.rows[ moveObjCoord[1] ].cells[ moveObjCoord[0] + this.finishID ]; // empty finished td 
			this.stepPerFps = percentSpeed/fps;
			this.stepPerFps = this.finishID*100/Math.floor(this.finishID*100/this.stepPerFps); // correct step if steppsCount is not equal
			this.activePercentShiftLeft = 0;
			
			this.objMoveFunc = setInterval(this.moveAction.bind(this), 1000/fps);
		}
	}		
		
	moveAction()
	{
		this.activePercentShiftLeft += this.stepPerFps;
		this.moveImg.style.left = (this.percentShiftLeft + this.activePercentShiftLeft).toString() + "%";
			
		if( this.activePercentShiftLeft >= this.finishID*100)
		{
			this.objTd.removeChild(this.moveObj);
			this.finishTd.appendChld(this.moveObj);
			
			this.playClass.moveObjCoord[0] += this.finishID;
			
			clearInterval(this.objMoveFunc);
		}
	}
	
}

class ActionLeft
{
	constructor(_buttonClass)
	{
		this.buttonClass = _buttonClass;
		this.imgPath = allActions["leftArrow"][0];

		this.playClass = this.buttonClass.playClass;
		this.moveObj = this.playClass.moveObj;
		this.objTr = this.playClass.table.rows[ this.playClass.moveObjCoord[1] ];
		this.objTd = this.objTr.cells[ this.playClass.moveObjCoord[0] ];
		this.finishID = undefined;
		this.finishCoord = undefiend;
		this.stepPerFps.undefined;
		
		this.percentShiftTop = this.playClass.moveObjImgLeftTopPercent[1];
		this.percentShiftLeft = this.playClass.moveObjImgLeftTopPercent[0];		
	}
	
	getImgPath()
	{
		return this.imgPath;
	}
	
	exec()
	{
		var moveObjCoord = this.playClass.moveObjCoord;
		if(moveObjCoord == undefined)
			return;		
		
		var borderInd = 1;		
		var borderTd = this.objTr.cells[ moveObjCoord[0]+borderInd ];
		
		if(borderTd.children.length == 0 && moveObjCoord[0]+borderInd < _tr.cells.length )
		{
			borderInd++;
		}
		
		this.finishID = borderInd-1;
		if(this.finishID > 0)
		{
			this.finishTd = this.playClass.table.rows[ moveObjCoord[1] ].cells[ moveObjCoord[0] + this.finishID ]; // empty finished td 
			this.stepPerFps = percentSpeed/fps;
			this.stepPerFps = this.finishID*100/Math.floor(this.finishID*100/this.stepPerFps); // correct step if steppsCount is not equal
			this.activePercentShiftLeft = 0;
			
			this.objMoveFunc = setInterval(this.moveAction.bind(this), 1000/fps);
		}
	}		
		
	moveAction()
	{
		this.activePercentShiftLeft += this.stepPerFps;
		this.moveImg.style.left = (this.percentShiftLeft + this.activePercentShiftLeft).toString() + "%";
			
		if( this.activePercentShiftLeft >= this.finishID*100)
		{
			this.objTd.removeChild(this.moveObj);
			this.finishTd.appendChld(this.moveObj);
			
			this.playClass.moveObjCoord[0] += this.finishID;			
			clearInterval(this.objMoveFunc);
		}
	}
	
}

class ActionAccRight
{
	constructor(_buttonClass)
	{
		this.buttonClass = _buttonClass;
		this.imgPath = allActions["rightArrow_acc"][0];

		this.playClass = this.buttonClass.playClass;
		this.moveObj = this.playClass.moveObj;
		this.objTr = this.playClass.table.rows[ this.playClass.moveObjCoord[1] ];
		this.objTd = this.objTr.cells[ this.playClass.moveObjCoord[0] ];
		this.finishID = undefined;
		this.finishCoord = undefiend;
		this.stepPerFps.undefined;
		
		this.percentShiftTop = this.playClass.moveObjImgLeftTopPercent[1];
		this.percentShiftLeft = this.playClass.moveObjImgLeftTopPercent[0];		
	}
	
	getImgPath()
	{
		return this.imgPath;
	}
	
	exec()
	{
		var moveObjCoord = this.playClass.moveObjCoord;
		if(moveObjCoord == undefined)
			return;		
		
		var borderInd = -1;		
		var borderTd = this.objTr.cells[ moveObjCoord[0]+borderInd ];
		
		while(borderTd.children.length == 0 && moveObjCoord[0]+borderInd >= 0 )
		{
			borderInd--;
			borderTd = this.objTr.cells[ moveObjCoord[0]+borderInd ];
		}
		
		this.finishID = borderInd+1;
		if(this.finishID < 0)
		{		
			this.finishTd = this.playClass.table.rows[ moveObjCoord[1] ].cells[ moveObjCoord[0] + this.finishID ]; // empty finished td 
			this.stepPerFps = percentSpeed/fps;
			this.stepPerFps = this.finishID*100/Math.floor(this.finishID*100/this.stepPerFps); // correct step if steppsCount is not equal
			this.activePercentShiftLeft = 0;
			
			this.objMoveFunc = setInterval(this.moveAction.bind(this), 1000/fps);
		}
	}		
		
	moveAction()
	{
		this.activePercentShiftLeft += this.stepPerFps;
		this.moveImg.style.left = (this.percentShiftLeft + this.activePercentShiftLeft).toString() + "%";
			
		if( this.activePercentShiftLeft <= this.finishID*100)
		{
			this.objTd.removeChild(this.moveObj);
			this.finishTd.appendChld(this.moveObj);
			
			this.playClass.moveObjCoord[0] += this.finishID;			
			clearInterval(this.objMoveFunc);
		}
	}
	
}

class ActionRight
{
	constructor(_buttonClass)
	{
		this.buttonClass = _buttonClass;
		this.imgPath = allActions["rightArrow"][0];

		this.playClass = this.buttonClass.playClass;
		this.moveObj = this.playClass.moveObj;
		this.objTr = this.playClass.table.rows[ this.playClass.moveObjCoord[1] ];
		this.objTd = this.objTr.cells[ this.playClass.moveObjCoord[0] ];
		this.finishID = undefined;
		this.finishCoord = undefiend;
		this.stepPerFps.undefined;
		
		this.percentShiftTop = this.playClass.moveObjImgLeftTopPercent[1];
		this.percentShiftLeft = this.playClass.moveObjImgLeftTopPercent[0];		
	}
	
	getImgPath()
	{
		return this.imgPath;
	}
	
	exec()
	{
		var moveObjCoord = this.playClass.moveObjCoord;
		if(moveObjCoord == undefined)
			return;		
		
		var borderInd = -1;		
		var borderTd = this.objTr.cells[ moveObjCoord[0]+borderInd ];
		
		if(borderTd.children.length == 0 && moveObjCoord[0]+borderInd >= 0 )
		{
			borderInd--;
		}
		
		this.finishID = borderInd+1;
		if(this.finishID < 0)
		{
			this.finishTd = this.playClass.table.rows[ moveObjCoord[1] ].cells[ moveObjCoord[0] + this.finishID ]; // empty finished td 
			this.stepPerFps = percentSpeed/fps;
			this.stepPerFps = this.finishID*100/Math.floor(this.finishID*100/this.stepPerFps); // correct step if steppsCount is not equal
			this.activePercentShiftLeft = 0;
			
			this.objMoveFunc = setInterval(this.moveAction.bind(this), 1000/fps);
		}
	}		
		
	moveAction()
	{
		this.activePercentShiftLeft += this.stepPerFps;
		this.moveImg.style.left = (this.percentShiftLeft + this.activePercentShiftLeft).toString() + "%";
			
		if( this.activePercentShiftLeft <= this.finishID*100)
		{
			this.objTd.removeChild(this.moveObj);
			this.finishTd.appendChld(this.moveObj);
			
			this.playClass.moveObjCoord[0] += this.finishID;
			clearInterval(this.objMoveFunc);
		}
	}
	
}

class ActionAccUp
{
	constructor(_buttonClass)
	{
		this.buttonClass = _buttonClass;
		this.imgPath = allActions["upArrow_acc"][0];

		this.playClass = this.buttonClass.playClass;
		this.moveObj = this.playClass.moveObj;
		this.objTr = this.playClass.table.rows[ this.playClass.moveObjCoord[1] ];
		this.objTd = this.objTr.cells[ this.playClass.moveObjCoord[0] ];
		this.finishID = undefined;
		this.finishCoord = undefiend;
		this.stepPerFps.undefined;
		
		this.percentShiftTop = this.playClass.moveObjImgLeftTopPercent[1];
		this.percentShiftLeft = this.playClass.moveObjImgLeftTopPercent[0];		
	}
	
	getImgPath()
	{
		return this.imgPath;
	}
	
	exec()
	{
		var moveObjCoord = this.playClass.moveObjCoord;
		if(moveObjCoord == undefined)
			return;		
		
		var borderInd = -1;		
		var borderTd = this.playClass.table.rows[ moveObjCoord[1]+borderInd ].cells[ moveObjCoord[0] ];
		
		while(borderTd.children.length == 0 && moveObjCoord[1]+borderInd >= 0 )
		{
			borderInd--;
			borderTd = this.playClass.table.rows[ moveObjCoord[1]+borderInd ].cells[ moveObjCoord[0] ];
		}
		
		this.finishID = borderInd+1;
		if(this.finishID < 0)
		{		
			this.finishTd = this.playClass.table.rows[ moveObjCoord[1] + this.finishID ].cells[ moveObjCoord[0] ]; // empty finished td 
			this.stepPerFps = percentSpeed/fps;
			this.stepPerFps = this.finishID*100/Math.floor(this.finishID*100/this.stepPerFps); // correct step if steppsCount is not equal
			this.activePercentShiftTop = 0;
			
			this.objMoveFunc = setInterval(this.moveAction.bind(this), 1000/fps);
		}
	}		
		
	moveAction()
	{
		this.activePercentShiftTop += this.stepPerFps;
		this.moveImg.style.top = (this.percentShiftTop + this.activePercentShiftTop).toString() + "%";
			
		if( this.activePercentShiftTop <= this.finishID*100)
		{
			this.objTd.removeChild(this.moveObj);
			this.finishTd.appendChld(this.moveObj);
			
			this.playClass.moveObjCoord[1] += this.finishID;			
			clearInterval(this.objMoveFunc);
		}
	}
	
}

class ActionUp
{
	constructor(_buttonClass)
	{
		this.buttonClass = _buttonClass;
		this.imgPath = allActions["upArrow"][0];

		this.playClass = this.buttonClass.playClass;
		this.moveObj = this.playClass.moveObj;
		this.objTr = this.playClass.table.rows[ this.playClass.moveObjCoord[1] ];
		this.objTd = this.objTr.cells[ this.playClass.moveObjCoord[0] ];
		this.finishID = undefined;
		this.finishCoord = undefiend;
		this.stepPerFps.undefined;
		
		this.percentShiftTop = this.playClass.moveObjImgLeftTopPercent[1];
		this.percentShiftLeft = this.playClass.moveObjImgLeftTopPercent[0];		
	}
	
	getImgPath()
	{
		return this.imgPath;
	}
	
	exec()
	{
		var moveObjCoord = this.playClass.moveObjCoord;
		if(moveObjCoord == undefined)
			return;		
		
		var borderInd = -1;		
		var borderTd = this.playClass.table.rows[ moveObjCoord[1]+borderInd ].cells[ moveObjCoord[0] ];
		
		if(borderTd.children.length == 0 && moveObjCoord[1]+borderInd >= 0 )
		{
			borderInd--;
		}
		
		this.finishID = borderInd+1;
		if(this.finishID < 0)
		{
			this.finishTd = this.playClass.table.rows[ moveObjCoord[1] + this.finishID ].cells[ moveObjCoord[0] ]; // empty finished td 
			this.stepPerFps = percentSpeed/fps;
			this.stepPerFps = this.finishID*100/Math.floor(this.finishID*100/this.stepPerFps); // correct step if steppsCount is not equal
			this.activePercentShiftTop = 0;
			
			this.objMoveFunc = setInterval(this.moveAction.bind(this), 1000/fps);
		}
	}		
		
	moveAction()
	{
		this.activePercentShiftTop += this.stepPerFps;
		this.moveImg.style.Top = (this.percentShiftTop + this.activePercentShiftTop).toString() + "%";
			
		if( this.activePercentShiftTop <= this.finishID*100)
		{
			this.objTd.removeChild(this.moveObj);
			this.finishTd.appendChld(this.moveObj);
			
			this.playClass.moveObjCoord[1] += this.finishID;			
			clearInterval(this.objMoveFunc);
		}
	}
}

class ActionAccDown
{
	constructor(_buttonClass)
	{
		this.buttonClass = _buttonClass;
		this.imgPath = allActions["downArrow_acc"][0];

		this.playClass = this.buttonClass.playClass;
		this.moveObj = this.playClass.moveObj;
		this.objTr = this.playClass.table.rows[ this.playClass.moveObjCoord[1] ];
		this.objTd = this.objTr.cells[ this.playClass.moveObjCoord[0] ];
		this.finishID = undefined;
		this.finishCoord = undefiend;
		this.stepPerFps.undefined;
		
		this.percentShiftTop = this.playClass.moveObjImgLeftTopPercent[1];
		this.percentShiftLeft = this.playClass.moveObjImgLeftTopPercent[0];		
	}
	
	getImgPath()
	{
		return this.imgPath;
	}
	
	exec()
	{
		var moveObjCoord = this.playClass.moveObjCoord;
		if(moveObjCoord == undefined)
			return;		
		
		var borderInd = 1;		
		var borderTd = this.playClass.table.rows[ moveObjCoord[1]+borderInd ].cells[ moveObjCoord[0] ];
		
		while(borderTd.children.length == 0 && moveObjCoord[1]+borderInd < 0 this.playClass.table.rows.length)
		{
			borderInd++;
			borderTd = this.playClass.table.rows[ moveObjCoord[1]+borderInd ].cells[ moveObjCoord[0] ];
		}
		
		this.finishID = borderInd+1;
		if(this.finishID > 0)
		{		
			this.finishTd = this.playClass.table.rows[ moveObjCoord[1] + this.finishID ].cells[ moveObjCoord[0] ]; // empty finished td 
			this.stepPerFps = percentSpeed/fps;
			this.stepPerFps = this.finishID*100/Math.floor(this.finishID*100/this.stepPerFps); // correct step if steppsCount is not equal
			this.activePercentShiftTop = 0;
			
			this.objMoveFunc = setInterval(this.moveAction.bind(this), 1000/fps);
		}
	}		
		
	moveAction()
	{
		this.activePercentShiftTop += this.stepPerFps;
		this.moveImg.style.top = (this.percentShiftTop + this.activePercentShiftTop).toString() + "%";
			
		if( this.activePercentShiftTop >= this.finishID*100)
		{
			this.objTd.removeChild(this.moveObj);
			this.finishTd.appendChld(this.moveObj);
			
			this.playClass.moveObjCoord[1] += this.finishID;			
			clearInterval(this.objMoveFunc);
		}
	}
	
}

class ActionDown
{
	constructor(_buttonClass)
	{
		this.buttonClass = _buttonClass;
		this.imgPath = allActions["downArrow"][0];

		this.playClass = this.buttonClass.playClass;
		this.moveObj = this.playClass.moveObj;
		this.objTr = this.playClass.table.rows[ this.playClass.moveObjCoord[1] ];
		this.objTd = this.objTr.cells[ this.playClass.moveObjCoord[0] ];
		this.finishID = undefined;
		this.finishCoord = undefiend;
		this.stepPerFps.undefined;
		
		this.percentShiftTop = this.playClass.moveObjImgLeftTopPercent[1];
		this.percentShiftLeft = this.playClass.moveObjImgLeftTopPercent[0];		
	}
	
	getImgPath()
	{
		return this.imgPath;
	}
	
	exec()
	{
		var moveObjCoord = this.playClass.moveObjCoord;
		if(moveObjCoord == undefined)
			return;		
		
		var borderInd = 1;		
		var borderTd = this.playClass.table.rows[ moveObjCoord[1]+borderInd ].cells[ moveObjCoord[0] ];
		
		if(borderTd.children.length == 0 && moveObjCoord[1]+borderInd < 0 this.playClass.table.rows.length )
		{
			borderInd++;
		}
		
		this.finishID = borderInd+1;
		if(this.finishID > 0)
		{
			this.finishTd = this.playClass.table.rows[ moveObjCoord[1] + this.finishID ].cells[ moveObjCoord[0] ]; // empty finished td 
			this.stepPerFps = percentSpeed/fps;
			this.stepPerFps = this.finishID*100/Math.floor(this.finishID*100/this.stepPerFps); // correct step if steppsCount is not equal
			this.activePercentShiftTop = 0;
			
			this.objMoveFunc = setInterval(this.moveAction.bind(this), 1000/fps);
		}
	}		
		
	moveAction()
	{
		this.activePercentShiftTop += this.stepPerFps;
		this.moveImg.style.Top = (this.percentShiftTop + this.activePercentShiftTop).toString() + "%";
			
		if( this.activePercentShiftTop >= this.finishID*100)
		{
			this.objTd.removeChild(this.moveObj);
			this.finishTd.appendChld(this.moveObj);
			
			this.playClass.moveObjCoord[1] += this.finishID;			
			clearInterval(this.objMoveFunc);
		}
	}
}