function nFormatter(num, digits) {
  var si = ["", "k", "M", "G", "T", "P", "E"];
  if (num == 0)
	  return 0;
  var expID;
  if(num < 10)
	  expID = 0;
  else
	expID = Math.floor(Math.floor(Math.log(num)/Math.log(10))/3);
  return (num / Math.pow(10, expID*3)).toFixed(digits) + si[expID];
}

var modalStack = [];
///////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////            BASE                      ////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////
class pObj
{
	constructor(elemType, _XobjID, _maxElements = undefined, hasContent = true) // 
	{
		this.pXobjID = true; // identificator parent as p<X> class
		
		this.parent = _parent;
		this.XobjID = _XobjID;
		if(hasContent)
		{
			this.objLeftPos = 5;
			this.objRightPos = 5;
			this.objTopPos = 5;
			this.objBottomPos = 5;
			this.objSpace = 5;
			
			this.pXobjTree = [];
		}
		else
		{
			this.objLeftPos = 0;
			this.objRightPos = 0;
			this.objTopPos = 0;
			this.objBottomPos = 0;
			this.objSpace = 0;
		}
		
		this.rows = 1;
		this.realRows = 1;	// unlimited param
		this.columns = 1; 	
		this.maxEelements = _maxElements; // max elements in tree may be unlimited if undefined
		
		this.Xobj = document.createElement(elemType);
		this.Xobj.id = this.XobjID;
		this.Xobj.style.position = "absolute";
		this.Xobj.style.padding = "0";
		this.Xobj.style.border = "0px solid white";				
	}
	
	setBorderStyle(_border)
	{
		if(this.Xobj)
			this.Xobj.style.border = _border;		
	}
	
	setRowColumn(_rows = 1, _columns = 1)
	{
		this.rows = _rows; 	// may be undefined		
		this.columns = _columns; // may be undefined			 
	}
	
	add(_parent, _insertInd = -1) 
	{
		this.parent = _parent;
		
		if(!this.parent.pXobjID) // this.parent is not pXobj class
		{
			this.parent.appendChild(this.Xobj);
			this.mainXobj = this;
		}
		else // this.parent is pXobj class
		{
			if(this.parent.maxEelements != undefined)
			{
				if(this.parent.pXobjTree.length + 1 >= this.parent.maxEelements)
					// exceed elements;
					return 0;
			}

			this.mainXobj = this.parent.mainXobj;
			this.parent.Xobj.appendChild(this.Xobj);
				
			if(insertInd != -1) 
				this.parent.pXobjTree.splice(insertInd, 0, this);
			else
				this.parent.pXobjTree.push(this);

		}	

		return 1;
	}
	
	setMinRowColumnSquareSize()
	{
		var objSquareX = 0, objSquareY = 0;		
		if(!this.parent.pXobjID) // this.parent is not pXobj class
		{		
			var maxObjX = this.Xobj.style.width;
			var maxObjY = this.Xobj.style.height;

			if(this.columns != undefined)
				objSquareX = Math.floor((maxObjX - (this.objLeftPos + this.objRightPos) - this.objSpace*(this.columns - 1)) / this.columns);
			else
				objSquareX = Math.floor(maxObjX - (this.objLeftPos + this.objRightPos));
			if(this.raws != undefined)
				objSquareY = Math.floor((maxObjY - (this.objTopPos + this.objBottomPos) - this.objSpace*(this.raws - 1)) / this.raws);
			else
				objSquareY = Math.floor(maxObjY - (this.objTopPos + this.objBottomPos));
		}
		else // this.parent is pXobj class
		{
			objSquareX = this.parent.minObjSquare - (this.objLeftPos + this.objRightPos);
			objSquareY = this.parent.minObjSquare - (this.objTopPos + this.objBottomPos);			
		}
		
		if(objSquareX > objSquareY)
			this.minObjSquare = objSquareY;
		else		
			this.minObjSquare = objSquareX;		
	}	
	
	resize()
	{
		var i=0;
		var curRow = 0, curColumn = 0;
		setMinRowColumnSquareSize();
		
		var childObjRowColumn = [];
		for(i = 0; i < this.pXobjTree.length; i++)
		{
			childObjRowColumn = this.pXobjTree[i].resize();
			if(childObjRowColumn[0] * childObjRowColumn[1] > 1)
			{
				if(curColumn > 0)
				{
					curRow++;
					curColumn = 0;
				}
			}
			
			this.pXobjTree[i].Xobj.style.top = this.objTopPos + curRow * (this.minObjSquare + this.objSpace);
			this.pXobjTree[i].Xobj.style.left = this.objLeftPos + curColumn * (this.minObjSquare + this.objSpace);
			
			if(childObjRowColumn[0] > 1)
			{
				curRow += (childObjRowColumn-1);
				curColumn = 0;
			}
			else
				curColumn++;
			
			this.pXobjTree[i].realRows = curRow;
			
			if(this.columns)
			{
				if(curColumn >= this.columns)
				{
					curRow++;
					curColumn = 0;
				}
			}
		}

		var objSize = setObjSize();
		
		return [this.Xobj.style.width, this.Xobj.style.height];
	}
	
}


	updateMainTreePosition()
	{
		if(this.maxElements != undefined)
		{
			if(this.pXobjTree > this.maxElements)
				return [false, {}];
		}
		
		var curRow = 0, curColumn = 0;
		var resultChildUpdate;
		
		var i = 0;
		
		this.pXobjTree[i].top = curRow;
		this.pXobjTree[i].left = curColumn;
		resultChildUpdate = this.pXobjTree[i].updateMainTreePosition();
		
		if(!resultChildUpdate[0])
			return [false, {}];
		
		this.pXobjTree[i].bottom = this.pXobjTree[i].top + resultChildUpdate[1]["bottom"];
		if(this.maxRaws != undefined)
		{
			if(this.pXobjTree[i].bottom >= this.maxRaws)
				return [false, {}];
		}
	 		
		this.pXobjTree[i].right = this.pXobjTree[i].left + resultChildUpdate[1]["right"];
		if(this.maxColumns != undefined)
		{
			if(this.pXobjTree[i].right >= this.maxColumns)
				return false;
		}
		
		curRow += this.pXobjTree[i].right + 1;
		curColumn += this.pXobjTree[i].bottom + 1;
		
		for(i = 1; i < this.pXobjTree.length; i++)
		{

		}
	}
	
	setCurXObjPos(XobjTreeInd)
	{
		var XobjChild = this.pXobjTree[XobjTreeInd];
		var XobjPrevChild = undefined;		
		var XobjChildLength = XobjChild.Rows * XobjChild.Columns;		
		
		if(XobjTreeInd == 0)
		{
			XobjChild.top = 0;
			XobjChild.left = 0;
		}
		else
		{
			XobjPrevChild = this.pXobjTree[XobjTreeInd - 1];
			if(XobjPrevChild.Rows > 1 || XobjPrevChild.Rows > 1)
			{
				XobjChild.left = 0;
				XobjChild.top = XobjPrevChild.top + XobjPrevChild.Rows;
			}
			else
			{
				if(XobjPrevChild.Rows > 1)
			}
			
			for(var j = 0; j < this.buttonRows; j++)
			{
				for(var i = 0; i < this.buttonColumns; i++)
				{
					if(this.objArr[i][j] == 0 || this.objArr[i][j] == undefined)
						return [i, j];
				}
			}
		}

		if (XobjChildLength > this.Columns)
		{
			XobjChild.Columns = this.Columns;
			XobjChild.Rows = Math.ceil(XobjChildLength / XobjChild.Columns);
		}
		
		return undefined;
	}

	addButton(buttonAction = undefined, coord = undefined)
	{
		if(coord == undefined)
			coord = this.getEmptyObjPos();
		
		if(coord == undefined)
			return;
		
		var btn = document.createElement("button");
		this.dialog.appendChild(btn);
		
//		if(buttonAction != undefined)
//			addImageToDivObj(btn, allActions[buttonAction][0]);
		
		if(coord[0] > this.buttonColumns)
		{
			this.buttonColumns = coord[0];
			this.initObjArr();
		}
		if(coord[1] > this.buttonRows)
			this.buttonRows = coord[1];

		btn.style.position = "absolute";

		this.objArr[ coord[0] ][ coord[1] ] = [btn, "button", undefined];
		
		if(buttonAction != undefined)
			this.objArr[ coord[0] ][ coord[1] ][2] = allActions[buttonAction][1];

		return btn;
	}
	
	resize(divMaxX, divMaxY)
	{
		var buttonX, buttonY;
		if(this.buttonColumns != 0)
			buttonX = Math.floor((		divMaxX 
									- 	this.objLeftPos 
									- 	this.objRightPos 
									- 	this.objSpace * (this.buttonColumns - 1))
									/	this.buttonColumns);
		else
			buttonX = 0;	

		if(this.buttonColumns != 0)		
			buttonY = Math.floor((		divMaxY
									- 	this.objTopPos 
									- 	this.objBottomPos 
									- 	this.objSpace * (this.buttonRows - 1))
									/ 	this.buttonRows); 									
		else
			buttonY = 0;
		
		if(buttonX > buttonY)
		{
			buttonX = buttonY;
		}

		var width =  this.objLeftPos;
		var height = this.objTopPos;
				
		for (var i = 0; i < this.buttonColumns; i++)
		{
			var heightMax = height;			
			for(var j = 0; j < this.buttonRows; j++)
			{
				var obj = this.objArr[i][j];
				if(obj != undefined)
				{
					if(obj[1] == "button")
					{
						obj[0].style.width = buttonX.toString() + "px";
						obj[0].style.height = buttonX.toString() + "px";
						
						obj[0].style.left = (this.objLeftPos + (buttonX + this.objSpace) * i).toString() + "px";
						obj[0].style.top = (this.objTopPos + (buttonX + this.objSpace) * j).toString() + "px";
					}
				}
				heightMax += buttonX + this.objSpace;
			}
			if(heightMax > height)
				height = heightMax;
			
			width += buttonX + this.objSpace;
		}
		
		width = width - this.objSpace + this.objRightPos;
		this.dialog.style.width =  toString() + "px";		

		height = height - this.objSpace + this.objBottomPos;
		this.dialog.style.height =  height.toString() + "px";
		
	}
	
}	

class pButton
{
	constructor(_parent, _playClass, _buttonClass, _actionClass, actionName = undefined)
	{
		this.parent = _parent;
		this.buttonClass = _buttonClass;
		this.playClass = _playClass;
		this.actionClass = _actionClass;
		
		this.curActionID = undefined;
		this.imgSrc = undefined;
		
		this.button = document.createElement("button");
		this.parent.appendChild(this.button);
		this.button.style.padding = "0";

		if(actionName != undefined)
		{
			this.action = this.registerAction(allActions[actionName][1]);
			this.addImage();
			
			this.button.addEventListener('click', this.btnClick.bind(this));
		}		
	}

	addImage(imgOpacity = "1")
	{
		if(this.action == undefined)
			return;
		
		var img = document.createElement("img");
		img.src = this.action.getImgPath();
		
		img.style.opacity = imgOpacity;
		img.style.width = "100%";
		img.style.height = "100%";
		this.button.appendChild(img);	
	}

	RemoveImage()
	{
		var img = this.button.children[0];
		this.button.removeChild(img);	
	}	

	btnClick()
	{
		if(this.action != undefined)
			this.actionClass.addAction(this.action);
	}
	
	resize(buttonWidth)
	{
		this.button.style.width = buttonWidth.toString() + "px";
		this.button.style.������ = buttonHeight.toString() + "px";
	}
	
	registerAction(actionID)
	{
		switch(actionID)
		{
			case 4: // accelerate left
				return new ActionAccLeft(this);
				break;
			case 0: // left
				return new ActionLeft(this);
				break;
			case 5: // accelerate right
				return new ActionAccRight(this);
				break;
			case 1: // right
				return new ActionRight(this);
				break;
			case 6: // accelerate up
				return new ActionAccUp(this);
				break;			
			case 2: // up
				return new ActionUp(this);
				break;
			case 7: // accelerate down
				return new ActionAccDown(this);
				break;				
			case 3: // down
				return new ActionAccDown(this);
				break;			
			default:
				return undefined;
		}
		
		return undefined;	
	}
}

class pTable
{
	constructor(_parent, _tableID, _labirynthArr = undefined)
	{
		this.parent = _parent;
		this.table = undefined;
		this.tableID = _tableID;
		this.labirynthArr = _labirynthArr;
	}

	addImageToTable(_td, imgPath, imgOpacity = "1")
	{
		var img = document.createElement("img");
		img.src = imgPath;
		img.style.opacity = imgOpacity;
		img.style.width = "100%";
		img.style.height = "100%";
		_td.appendChild(img);	
	}

	RemoveImageFromTable(_td)
	{
		var img = _td.children[0];
		_td.removeChild(img);	
	}
	
	create(rows = 3, columns = 3, _style = "1px solid #000000")
	{
		
		this.table = document.createElement('table');
		this.parent.appendChild(this.table);
		this.table.id = this.tableID;
		this.table.style.borderCollapse = "collapse";
		this.table.style.position = "absolute";
		this.table.parent = this.parent;
		this.parentClass = this;
		this.table.rowMax = rows;
		this.table.columnMax = columns;

		if(this.labirynthArr)
		{
			rows = this.labirynthArr.length;
			this.table.rowMax = rows;
			this.table.columnMax = this.labirynthArr[0].length;
		}
		
		for (var i=0; i< rows; i++)
		{
			var _tr = this.table.appendChild(document.createElement('tr'));
			if(this.labirynthArr)
			{
				columns = this.labirynthArr[i].length;
				if(this.table.columnMax < columns)
					this.table.columnMax = columns;					
			}
			
			for(var j=0; j<columns; j++)
			{
					var _td = _tr.appendChild(document.createElement('td'));
					_td.style.border = _style;
					_td.style.padding = "0";				
					if(this.labirynthArr)
					{
						if(this.labirynthArr[i][j] == 1)
						{
							this.addImageToTable(_td, "./img/fill.png");
						}
					}
			}
		}
	}
	
	resize(tableMaxX, tableMaxY)
	{
		var _td;
		var squareY = Math.floor(tableMaxY / this.table.rowMax);
		var squareX = Math.floor(tableMaxX / this.table.columnMax);
		if(squareX > squareY)
			squareX = squareY;
			
		for (var i = 0; i < this.table.rows.length; i++)
		{
			for(var j = 0; j < this.table.rows[i].cells.length; j++)
			{
				_td = this.table.rows[i].cells[j];
				if(_td)
				{
					_td.style.width = squareX.toString() + "px";
					_td.style.height = squareX.toString() + "px";
				}
			}
		}		
	}	
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////


	
class ChooseActionDiv extends pDiv
{
	constructor(_parent, posX, posY)
	{
		var id = "actionDiv";
		super(_parent, id);
		resizedElements.push(id);
		
		this.x = posX;
		this.y = posY;
		this.redraw();
		
		this.dialog.padding = "0";
		this.dialog.style.top = this.y.toString() + "px";
		this.dialog.style.right = this.x.toString() + "px";
	
		this.dialog.resize = this.resize.bind(this);	
		
	}
	
	redraw()
	{
		super.create(5, 2);
//		for(var i=0; i < 8; i++)
//			this.addButton();
		
		this.resize();
	}
	
	resize()
	{
		var tableMaxX = window.innerWidth * 0.30 - this.x;
		var tableMaxY = window.innerHeight * 0.65 - this.y;
		
		super.resize(tableMaxX, tableMaxY);
	}
}

class ActionSequenceDiv extends pDiv
{
	constructor(_parent, posX, posY)
	{
		var id = "sequenceTable";
		super(_parent, id);
		resizedElements.push(id);
		
		this.x = posX;
		this.y = posY;
		this.redraw();
		
		this.dialog.padding = "0";
		this.dialog.style.bottom = this.y.toString() + "px";
		this.dialog.style.left = this.x.toString() + "px";
		
		this.dialog.resize = this.resize.bind(this);
	
	}
	
	redraw()
	{
		super.create(1, 10, "1px dashed DarkGrey");
	}
	
	resize()
	{
		var tableMaxX = window.innerWidth * 0.65 - this.x;
		var tableMaxY = window.innerHeight * 0.30 - this.y;
		
		super.resize(tableMaxX, tableMaxY);
	}
	
}



	
/*	
		this.top = 20;
		this.left = 20;
		this.width = 40;
		this.height = 40;

		this.dialog = document.createElement("div");
		this.parent.appendChild(this.dialog);		
		
		this.dialog.id = this.modalID;
		
		this.dialog.style.position = "absolute"; 
		this.dialog.style.zIndex = "1"; 
		
		this.dialog.style.width = this.width.toString() + "%"; 
		this.dialog.style.height = this.height.toString() + "%"; 
		this.dialog.style.overflow = "auto";
		this.dialog.style.borderStyle = "solid";
		this.dialog.style.backgroundColor = "white";
		
		this.dialog.parent = this.parent;
		this.dialog.parentClass = this;
		
		this.dialog.resize = this.resize.bind(this);
		
		this.redraw();
		

	}
	
	redraw() // true false
	{

		this.dialog.style.display = "block";
					
		if(modalStack.length == 0)
		{
			this.dialog.style.left = this.left.toString() + "%";
			this.dialog.style.top = this.top.toString() + "%";
		}
		else
		{
			this.dialog.style.left = (this.left + 5*modalStack.length).toString() + "%";
			this.dialog.style.top = (this.top + 5*modalStack.length).toString() + "%";
		}
	}
	
	resize()
	{
		
	}
	
	close()
	{
		this.parent.removeChild(this.dialog);
	}
}
*/

var resizedElements = [];

class PlaygroundTable extends pTable
{
	constructor(_parent, level, posX, posY)
	{
		var id = "playgroundTable";
		super(_parent, id, levelConfig[level]["design"]);
		resizedElements.push(id);
		
		this.x = posX;
		this.y = posY;
		
		this.moveObjCoord = levelConfig[level]["start"];
		this.moveObjImg = levelConfig[level]["obj"][0];
		this.moveObjImgLeftTopPercent = [10, 10];

		
		this.redraw(level);
		this.table.resize = this.resize.bind(this);
		
	}
	
	redraw(level)
	{
		super.create(20, 20, "0px dashed DarkGrey");
		
		this.table.padding = "0";
		this.table.style.top = this.y.toString() + "px";
		this.table.style.left = this.x.toString() + "px";

		this.moveObj = document.createElement("img");		
		var _td = this.table.rows[ this.moveObjCoord[1] ].cells[ this.moveObjCoord[0] ];
		_td.appendChild(this.moveObj);
		this.moveObj.style.width = (100 - 2*this.moveObjImgTopLeftPercent[0]).toString() + "%";
		this.moveObj.style.height = (100 - 2*this.moveObjImgTopLeftPercent[1]).toString() + "%";;
		this.moveObj.src = this.moveObjImg;
		this.moveObj.style.position = "absolute";
		this.moveObj.style.left = this.moveObjImgTopLeftPercent[0].toString() + "%";
		this.moveObj.style.top = this.moveObjImgTopLeftPercent[1].toString() + "%";		
	}
	
	resize()
	{
		var tableMaxX = window.innerWidth * 0.65 - this.x;
		var tableMaxY = window.innerHeight * 0.65 - this.y;
		
		super.resize(tableMaxX, tableMaxY);
	}
	
}

class ChooseActionTable extends pTable
{
	constructor(_parent, posX, posY)
	{
		var id = "actionTable";
		super(_parent, id);
		resizedElements.push(id);
		
		this.x = posX;
		this.y = posY;
		this.redraw();
		
		this.table.padding = "0";
		this.table.style.top = this.y.toString() + "px";
		this.table.style.right = this.x.toString() + "px";
	
		this.table.resize = this.resize.bind(this);	
		
	}
	
	redraw()
	{
		super.create(5, 2, "1px dashed DarkGrey");
	}
	
	resize()
	{
		var tableMaxX = window.innerWidth * 0.30 - this.x;
		var tableMaxY = window.innerHeight * 0.65 - this.y;
		
		super.resize(tableMaxX, tableMaxY);
	}
	
}


class ActionSequenceTable extends pTable
{
	constructor(_parent, posX, posY)
	{
		var id = "sequenceTable";
		super(_parent, id);
		resizedElements.push(id);
		
		this.x = posX;
		this.y = posY;
		this.redraw();
		
		this.table.padding = "0";
		this.table.style.bottom = this.y.toString() + "px";
		this.table.style.left = this.x.toString() + "px";
		
		this.table.resize = this.resize.bind(this);
	
		this.actionSequence = [];
	}
	
	redraw()
	{
		super.create(2, 5, "1px dashed DarkGrey");
	}
	
	resize()
	{
		var tableMaxX = window.innerWidth * 0.65 - this.x;
		var tableMaxY = window.innerHeight * 0.30 - this.y;
		
		super.resize(tableMaxX, tableMaxY);
	}
	
	addAction(actionClass)
	{
		
	}
	
}

class Body
{
	constructor(bodyID)
	{
		this.body = document.createElement('body');
		this.body.id = bodyID;
		document.documentElement.appendChild(this.body);
		this.body.style.overflow = "hidden";		
		this.body.style.padding = "0";
		
		this.labyrinthLevel = 2;
		var pos = {"top": 5, "right": 5, "left": 5, "bottom": 5};
		this.playgroundTable = new PlaygroundTable(this.body, this.labyrinthLevel, pos.left, pos.top);
//		this.chooseActionTable = new ChooseActionTable(this.body, pos.right, pos.top);
		this.chooseActionTable = new ChooseActionDiv(this.body, pos.right, pos.top);		
//		this.actionSequenceTable = new ActionSequenceTable(this.body, pos.left, pos.bottom);
		this.actionSequenceTable = new ActionSequenceDiv(this.body, pos.left, pos.bottom);
		
		
		window.onresize = this.resize.bind(this);
		window.onresize();		
		
	}
	
	resize()
	{
		this.playgroundTable.resize();
		this.chooseActionTable.resize();
		this.actionSequenceTable.resize();
	}
}



/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
function addImageToTable(_td, imgPath, imgOpacity = "1")
	{
		var img = document.createElement("img");
		img.src = imgPath;
		img.style.opacity = imgOpacity;
		img.style.width = "100%";
		img.style.height = "100%";
		_td.appendChild(img);	
	}

function RemoveImageFromTable(_td)
	{
		var img = _td.children[0];
		_td.removeChild(img);	
	}

function modalResize(bodyID, modalDialogID)
{
	var modalDialog = document.getElementById(modalDialogID);
	if(modalDialog)
	{
		var bodyHeight = parseInt(document.getElementById(bodyID).style.height, 10);	
		var bodyWidth = parseInt(document.getElementById(bodyID).style.width, 10);
		modalDialog.style.top = Math.floor(bodyHeight*0.3).toString() + "px";
		modalDialog.style.left = Math.floor(bodyWidth*0.3).toString() + "px";
		modalDialog.style.height = Math.floor(bodyHeight*0.4).toString() + "px";
		modalDialog.style.width = Math.floor(bodyWidth*0.4).toString() + "px";
	}
}

function getEmptyTd(table)
{
	for(var i = 0; i < table.rows.length; i++)
	{
		for(var j=0; j < table.rows[i].cells.length; j++)
		{
			if (table.rows[i].cells[j].children.length == 0)
				return table.rows[i].cells[j];
		}
	}
	return undefined;
}

class actionFor extends pDiv
{
	constructor(_parent, _activateTd)
	{
		super(_parent, "modal" + modalStack.length.toString());
		
		this.activateTd = _activateTd;
		modalStack.push(this);

		var a = document.createElement("button");
		a.innerHTML = "asdasd";
		this.dialog.appendChild(a);
		
		a.addEventListener('click', this.close.bind(this));
		
	}
	
	close()
	{
		modalStack.pop();
		super.close();
	}
}


class actionSequence
{
	constructor(_parent, _buttonClass, _actionClass, _moveImg, _startCoord)
	{
		this.parent = _parent;
		this.goBtn = undefined;
		this.startCoord = _startCoord;
		this.moveImg = _moveImg;
		this.buttonClass = _buttonClass;
		this.actionClass = _actionClass;
		this.actionButtonArr = [];
		this.actionSequenceArr = [];
		this.actionSequenceIDArr = [];
		this.curSequence = 0;

		for(var i=0; i < levelConfig[level]["actions"].length; i++)
		{
			var newButton = this.buttonClass.addButton(levelConfig[level]["actions"][i]));
			this.actionButtonArr[i] = newButton;
			newButton.addEventListener('click', this.)
		}
	}
	
	removeActionSequence()
	{
		for(var i = 0; i < this.actionSequenceArr.length; i++)
		{
			RemoveImageFromTable(this.actionSequenceArr[i].td);
			this.actionSequenceArr[i].td.style.backgroundColor = "initial";
		}
		this.actionSequenceArr = [];
		this.removeGoButton();
	}
	
	addGoButton()
	{
		this.goBtn = document.createElement('button');
		var _tdBtn = this.actionTable.rows[0].appendChild(document.createElement('td'));
		_tdBtn.style.padding = "0";
		this.goBtn.innerHTML = "GO";		
		this.goBtn.style.textShadow = "2px 2px 4px #21701B";
		this.goBtn.style.fontStyle = "italic";
		this.goBtn.style.fontSize = "150%";
		
		this.goBtn.style.backgroundColor = "#43D037";
		this.goBtn.style.position = "relative";
		this.goBtn.style.top = "50%";
		this.goBtn.style.left = "10%";
		this.goBtn.style.width = "200%";
		this.goBtn.style.height = "200%";
		_tdBtn.appendChild(this.goBtn);
		this.actionTable.resize();
		this.goBtn.addEventListener('click', this.runSequence.bind(this));
	}
	
	removeGoButton()
	{
		var _tdGoBtn = this.actionTable.rows[0].cells[this.actionTable.rows[0].cells.length-1];
		this.actionTable.rows[0].removeChild(_tdGoBtn);
	}
	
	runSequence()
	{
		this.fps = 25;		
		this.percentSpeed = 200; //percent per sec	
		this.percentShiftTop = 0;
		this.percentShiftLeft = 0;
			
		this.objMoveFunc = setInterval(this.moveAction.bind(this), 1000/this.fps);
	}

	moveAction()
	{
	//	leftArrow 		0
	//	rightArrow 		1
	//	upArrow 		2
	//	downArrow 		3
	// leftArrow_acc 	4
	// rightArrow_acc 	5
	// upArrow_acc 		6
	// downArrow_acc 	7
		if( this.curSequence >= this.actionSequenceArr.length)
		{
			this.percentShiftTop = 0;
			this.percentShiftLeft = 0;			 
			this.curSequence = 0;
			this.removeActionSequence();
			clearInterval(this.objMoveFunc);
			return;
		}	
	
		var actionSequenceTd = this.actionSequenceArr[this.curSequence].td;
		actionSequenceTd.style.backgroundColor = "#EFAD8D";
		
		var _td = this.moveImg.parentElement;
		var table = _td.parentElement.parentElement;
		var _tdx = _td.cellIndex;
		var _tdy = _td.parentElement.rowIndex;
		var timePerFps = 1/this.fps;

		var nextCoord = [0, 0];
		var isAccelerate = false;
		switch(this.actionSequenceArr[this.curSequence].actionID)
		{
			case 4: // accelerate left
				isAccelerate = true;
			case 0: // left
				nextCoord[0] = -1;
				break;
			case 5: // accelerate right
				isAccelerate = true;
			case 1: // right
				nextCoord[0] = 1;
				break;
			case 6: // accelerate up
				isAccelerate = true;				
			case 2: // up
				nextCoord[1] = -1;
				break;
			case 7: // accelerate down
				isAccelerate = true;				
			case 3: // down
				nextCoord[1] = 1;
				break;			
			default:
				nextCoord = [0, 0];
		}

		if(Math.abs(this.percentShiftTop) >= 100 || Math.abs(this.percentShiftLeft) >= 100)
		{
			this.percentShiftTop = 0;
			this.percentShiftLeft = 0;
			this.moveImg.style.top = "0%";
			this.moveImg.style.left = "0%";

			if(table.rows[_tdy + nextCoord[1]].cells[_tdx + nextCoord[0]].children.length == 0)	// empty
			{		
				_td.removeChild(this.moveImg);			
				table.rows[_tdy + nextCoord[1]].cells[_tdx + nextCoord[0]].appendChild(this.moveImg);
			}
			else if(isAccelerate == true)
			{	
				isAccelerate = false;
			}			
			
			if(isAccelerate == false)
			{	
				this.curSequence++;
			}

			this.moveAction();			
			return;
		}		
		
		this.percentShiftLeft += Math.floor(nextCoord[0]*this.percentSpeed*timePerFps);
		this.percentShiftTop += Math.floor(nextCoord[1]*this.percentSpeed*timePerFps);

		if(table.rows[_tdy + nextCoord[1]].cells[_tdx + nextCoord[0]].children.length == 0)	// empty
		{
			this.moveImg.style.top = this.percentShiftTop.toString() + "%";
			this.moveImg.style.left = this.percentShiftLeft.toString() + "%";
		}
	}
}

/*
class actionSequence
{
	constructor(_buttonTable, _actionTable, _moveImg, _startCoord)
	{
		this.goBtn = undefined;
		this.startCoord = _startCoord;
		this.moveImg = _moveImg;
		this.buttonTable = _buttonTable;
		this.actionTable = _actionTable;
		this.actionButtonArr = [];
		this.actionSequenceArr = [];
		this.actionSequenceIDArr = [];
		this.curSequence = 0;

		for(var i=0; i < levelConfig[level]["actions"].length; i++)
		{
			this.actionButtonArr[i] = new actionButton(	this, 
														this.buttonTable, 
														this.actionTable, 
														levelConfig[level]["actions"][i]);
		}
	}
	
	removeActionSequence()
	{
		for(var i = 0; i < this.actionSequenceArr.length; i++)
		{
			RemoveImageFromTable(this.actionSequenceArr[i].td);
			this.actionSequenceArr[i].td.style.backgroundColor = "initial";
		}
		this.actionSequenceArr = [];
		this.removeGoButton();
	}
	
	addGoButton()
	{
		this.goBtn = document.createElement('button');
		var _tdBtn = this.actionTable.rows[0].appendChild(document.createElement('td'));
		_tdBtn.style.padding = "0";
		this.goBtn.innerHTML = "GO";		
		this.goBtn.style.textShadow = "2px 2px 4px #21701B";
		this.goBtn.style.fontStyle = "italic";
		this.goBtn.style.fontSize = "150%";
		
		this.goBtn.style.backgroundColor = "#43D037";
		this.goBtn.style.position = "relative";
		this.goBtn.style.top = "50%";
		this.goBtn.style.left = "10%";
		this.goBtn.style.width = "200%";
		this.goBtn.style.height = "200%";
		_tdBtn.appendChild(this.goBtn);
		this.actionTable.resize();
		this.goBtn.addEventListener('click', this.runSequence.bind(this));
	}
	
	removeGoButton()
	{
		var _tdGoBtn = this.actionTable.rows[0].cells[this.actionTable.rows[0].cells.length-1];
		this.actionTable.rows[0].removeChild(_tdGoBtn);
	}
	
	runSequence()
	{
		this.fps = 25;		
		this.percentSpeed = 200; //percent per sec	
		this.percentShiftTop = 0;
		this.percentShiftLeft = 0;
			
		this.objMoveFunc = setInterval(this.moveAction.bind(this), 1000/this.fps);
	}

	moveAction()
	{
	//	leftArrow 		0
	//	rightArrow 		1
	//	upArrow 		2
	//	downArrow 		3
	// leftArrow_acc 	4
	// rightArrow_acc 	5
	// upArrow_acc 		6
	// downArrow_acc 	7
		if( this.curSequence >= this.actionSequenceArr.length)
		{
			this.percentShiftTop = 0;
			this.percentShiftLeft = 0;			 
			this.curSequence = 0;
			this.removeActionSequence();
			clearInterval(this.objMoveFunc);
			return;
		}	
	
		var actionSequenceTd = this.actionSequenceArr[this.curSequence].td;
		actionSequenceTd.style.backgroundColor = "#EFAD8D";
		
		var _td = this.moveImg.parentElement;
		var table = _td.parentElement.parentElement;
		var _tdx = _td.cellIndex;
		var _tdy = _td.parentElement.rowIndex;
		var timePerFps = 1/this.fps;

		var nextCoord = [0, 0];
		var isAccelerate = false;
		switch(this.actionSequenceArr[this.curSequence].actionID)
		{
			case 4: // accelerate left
				isAccelerate = true;
			case 0: // left
				nextCoord[0] = -1;
				break;
			case 5: // accelerate right
				isAccelerate = true;
			case 1: // right
				nextCoord[0] = 1;
				break;
			case 6: // accelerate up
				isAccelerate = true;				
			case 2: // up
				nextCoord[1] = -1;
				break;
			case 7: // accelerate down
				isAccelerate = true;				
			case 3: // down
				nextCoord[1] = 1;
				break;			
			default:
				nextCoord = [0, 0];
		}

		if(Math.abs(this.percentShiftTop) >= 100 || Math.abs(this.percentShiftLeft) >= 100)
		{
			this.percentShiftTop = 0;
			this.percentShiftLeft = 0;
			this.moveImg.style.top = "0%";
			this.moveImg.style.left = "0%";

			if(table.rows[_tdy + nextCoord[1]].cells[_tdx + nextCoord[0]].children.length == 0)	// empty
			{		
				_td.removeChild(this.moveImg);			
				table.rows[_tdy + nextCoord[1]].cells[_tdx + nextCoord[0]].appendChild(this.moveImg);
			}
			else if(isAccelerate == true)
			{	
				isAccelerate = false;
			}			
			
			if(isAccelerate == false)
			{	
				this.curSequence++;
			}

			this.moveAction();			
			return;
		}		
		
		this.percentShiftLeft += Math.floor(nextCoord[0]*this.percentSpeed*timePerFps);
		this.percentShiftTop += Math.floor(nextCoord[1]*this.percentSpeed*timePerFps);

		if(table.rows[_tdy + nextCoord[1]].cells[_tdx + nextCoord[0]].children.length == 0)	// empty
		{
			this.moveImg.style.top = this.percentShiftTop.toString() + "%";
			this.moveImg.style.left = this.percentShiftLeft.toString() + "%";
		}
	}
}
*/

var h = document.createElement('head');
h.innerHTML = "test";
document.documentElement.appendChild(h);


var b = new Body("mainBody");

// b.playgroundTable;
// b.chooseActionTable;
// b.actionSequenceTable;

var img = document.createElement("img");

var level = 2;
b.playgroundTable.table.rows[levelConfig[level]["start"][0]].cells[levelConfig[level]["start"][1]].appendChild(img);	
img.style.width = "80%";
img.style.height = "80%";
img.src = "./img/active_obj.png";
img.style.position = "relative";
//img.style.left = "2px";
//img.style.top = "20px";

var actions = new actionSequence(b, b.chooseActionTable, b.actionSequenceTable, img, levelConfig[level]["start"]);


/////////////// test modal window
//var _testModalTd = b.chooseActionTable.table.rows[0].cells[0];
//_testModalTd.children[0].addEventListener('click', function(){ var dialog = new actionFor(b.body, _testModalTd); } );		

